# AngularjsTestProject

## Getting Started

All you need to do is to install the dependencies using:

```
npm install
```

Then, run the Application:

```
node index.js
```

You can access your app at

```
http://localhost:8000/
```
