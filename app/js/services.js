angular.module('DemoApp.services', [])
  .factory('ergastAPIservice', function($http) {

    var ergastAPI = {};

    
    // ergastAPI.getDrivers = function() {
    //   var success = function (response) {
    //     console.log(response.data);
    //     return response.data;
    //   }
    //   var error = function(reason){
    //     console.log(reason.data);
    //   }
    //    $http({
    //     method: 'GET',
    //     url: 'http://ergast.com/api/f1/2013/driverStandings.json?callback=JSON_CALLBACK'
    //   }).then(success, error);
    // }
    ergastAPI.getDriverDetails = function(id) {
      return $http({
        method: 'GET',
        url: 'http://ergast.com/api/f1/2013/drivers/'+ id +'/driverStandings.json?callback=JSON_CALLBACK'
      });
    }

    ergastAPI.getDriverRaces = function(id) {
      return $http({
        method: 'GET',
        url: 'http://ergast.com/api/f1/2013/drivers/'+ id +'/results.json?callback=JSON_CALLBACK'
      });
    }

    return ergastAPI;
  });
