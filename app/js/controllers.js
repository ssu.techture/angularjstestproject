angular.module('DemoApp.controllers', []).

  /* Drivers controller */
  controller('driversController', function($scope, ergastAPIservice, $http) {
    $scope.nameFilter = null;
    $scope.driversList = [];
    $scope.searchFilter = function (driver) {
        var re = new RegExp($scope.nameFilter, 'i');
        return !$scope.nameFilter || re.test(driver.Driver.givenName) || re.test(driver.Driver.familyName);
    };

   var getDrivers = function() {
     var success = function (response) {
       console.log(response.data);
      $scope.driversList = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
     }
     var error = function(reason){
       console.log(reason.data);
     }
      $http({
       method: 'GET',
       url: 'http://ergast.com/api/f1/2013/driverStandings.json'
     }).then(success, error);
   }
   getDrivers();
 })

  /* Driver controller */
  // .controller('driverController', function($scope, $routeParams, ergastAPIservice) {
  //   $scope.id = $routeParams.id;
  //   $scope.races = [];
  //   $scope.driver = null;
  //   var t2 = ergastAPIservice.getDriverDetails($scope.id);
  //   if (t2) {
  //       $scope.driver = response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
  //       console.log($scope.driver);
  //   }
  //   // ergastAPIservice.getDriverDetails($scope.id).success(function (response) {
  //   //     $scope.driver = response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
  //   // });
  //
  //   ergastAPIservice.getDriverRaces($scope.id).success(function (response) {
  //       $scope.races = response.MRData.RaceTable.Races;
  //   });
  // });
